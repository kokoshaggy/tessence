��    J      l  e   �      P     Q     `     g          �  ,   �     �                          2     B     Q     h     t     �     �     �     �     �     �     �     �     �     �     	          +     3  <   L     �  
   �     �     �     �     �     �     �     	     "	     /	     <	     \	     p	     ~	     �	     �	     �	     �	     �	  k   �	     3
     :
     K
     `
  !   f
  #   �
  +   �
     �
  	   �
     �
  	   �
     �
                0     H     d     h     o     �     �  �  �     #     9     ?  /   W  &   �  /   �  .   �       	   %     /     6     M     b     x     �     �     �     �     �     �               #     7     H     P     m     �     �  "   �  U   �  )   (     R     ^     s     �  
   �     �     �     �     �     �          (     E  	   [     e     x     �     �     �  z   �  	   .     8     M     i  (   p  '   �  A   �  	                     	   '     1     H     ^     }     �     �     �     �     �         2   ,   .   8          (       $           '      	      &      J      #                          +   D   "   ;   6   7       !             A                 ?            =      -       B   H   :          /   G          1      >           
                       F   3   4   <         )   @   %   0   E           I             *              9       C   5              All Categories Author Choose background color Choose color for discount text Choose inside border color Choose the expiration method for this coupon Click to Open in Print View Click to Print Content Coupon Coupon Categories Coupon Category Coupon Creator Coupon Creator Options Coupon Deal Coupon Fields Coupon Loop Coupon Post Title Coupon Shortcode Coupon Terms Coupon Title Coupons Date (default) Date Format Deal Deal Background Color Deal Field Colors Deal Text Color Display Enter Coupon Admin Title Enter coupon deal - 30% OFF! or Buy One Get One Free, etc... Enter the terms of the discount Expiration Expiration Date Expiration Option Expires in X Days Expires on: Help Ignore Expiration Ignore Expiration Date Image Coupon Inner Border Insert Coupon Creator Shortcode Inside Border Color Last Modified Licenses No Categories No Coupons are Published None Not Showing Options Place this coupon in your posts, pages, custom post types, or widgets by using the shortcode below:<br><br> Random Range Expiration Recurring Expiration Reset Select How to Align the Coupon(s) Select Loop or an Individual Coupon Select a Coupon Category to use in the Loop Showing Slug Name Style Support:  Terms This Coupon Expired On  This Coupon Expires On  This Coupon is Showing. This Coupon is not Showing. Yes coupon coupon categories coupon category coupons Project-Id-Version: Coupon Creator
POT-Creation-Date: 2016-11-03 12:30-0400
PO-Revision-Date: 2016-11-03 12:30-0400
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ..
X-Poedit-WPHeader: coupon_creator.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
X-Poedit-SearchPathExcluded-1: .git
X-Poedit-SearchPathExcluded-2: node_modules
X-Poedit-SearchPathExcluded-3: sass
X-Poedit-SearchPathExcluded-4: vendor
X-Poedit-SearchPathExcluded-5: languages
X-Poedit-SearchPathExcluded-6: plugin-engine
 Todas las Categorías Autor Elija el color de fondo Seleccione un color para el texto del descuento Seleccione el color del borde interior Elegir el método de caducidad para este cupón Haga Clic en Para Abrir la Vista de Impresión Haga Clic Para Imprimir Contenido Cupón Categorías de cupones Categoría de cupón Creador de cupón Pro Opciones de Creador de Cupón Promoción del Cupón Campos del Cupón Lazo de cupón Título del cupón Shortcode de cupón Términos de cupón Título del Cupón Cupones Fecha (por defecto) Formato de Fecha Acuerdo Color de fondo del descuento Oferta campo colores Color del texto del descuento Visualización Título de administrador de cupón Introduzca la promoción - 30% de Descuento! o Compre uno y tenga otro gratis, etc... Introducir en los términos del descuento Expiración Fecha de expiración Opción de vencimiento Caduca dentro de X días Expira el: Ayuda Ignorar fecha de caducidad Ignorar fecha de caducidad Imagen del Cupón Borde interior Inserte shortcode del cupón Color del interior del borde Última modificación Licencias No hay categorías No hay cupones publicados Ninguna No mostrándose Opciones Coloca este cupón en tus posts, páginas, tipos de post personalizados o widgets utilizando el código siguiente:<br><br> Aleatorio Rango de vencimiento Recurrentes de la caducidad Borrar Seleccione la que desea alinear el cupon Seleccione bucle o un cupón Individual Seleccione una categoría de cupones para utilizar en el circuito Mostrando Sula Nombre Estilo Apoyo: Términos Este cupón expiró el Este cupón expira el Este cupón se está mostrando Este cupon no está mostrandose Si cupón categorías de cupones Categoría de cupón cupones 