<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

// END ENQUEUE PARENT ACTION

if ( ! function_exists( 'et_get_footer_credits' ) ) :
function et_get_footer_credits() {
		
	$disable_custom_credits = et_get_option( 'disable_custom_footer_credits', false );

	if ( $disable_custom_credits ) {
		return '';
	} else {
		return '<p id="footer-info">Designed by Epsolun</p>';
	}

}
endif;

/***********************Boffins Systems Modifications ********************************/

//Short code added by Anthony and Kingsley
get_the_post_thumbnail( $page->ID, 'thumbnail' );
function my_cat_image_shortcode($atts){

    $a = shortcode_atts( array(
        'id' => 1,
    ), $atts );
	
	$category_term = get_term($a['id'], 'product_cat');
    //$get_image=wp_get_attachment_url(56);
    
    
    $idcat = $a['id'];
	$thumbnail_id = get_woocommerce_term_meta( $idcat, 'thumbnail_id', true );
	$image = wp_get_attachment_url( $thumbnail_id );
    
    
	$description = $category_term->description;
    
    return $image;//$description.$image;
}


function my_cat_description_shortcode($atts){

    $a = shortcode_atts( array(
        'id' => 1,
    ), $atts );
	
	$category_term = get_term($a['id'], 'product_cat');
	$description = $category_term->description;
    
    return $description;
}


function my_cat_name_shortcode($atts){

    $a = shortcode_atts( array(
        'id' => 1,
    ), $atts );
	
	$category_term = get_term($a['id'], 'product_cat');
	$name = $category_term->name;
    
    return $name;
}


function general_link(){
    $plinks =get_permalink(); //$category_term->description;
    return $plinks;
}
 
/*function my_cat_description_shortcode2($atts){
    

    $a = shortcode_atts( array(
        'id' => 1,
    ), $atts );
	
	//$category_term = get_term($a['id'], 'product_cat');
    $post_7 = get_post( $a['id'], ARRAY_A );
    $title = $post_7['post_title'];
	$description ="<div>".get_the_post_thumbnail( $a['id'], 'thumbnail' )."</div>".$title; //$category_term->description;
    
    return $description;
}
**/

add_shortcode('cat_description', 'my_cat_description_shortcode');
add_shortcode('cat_name', 'my_cat_name_shortcode');
add_shortcode('cat_image', 'my_cat_image_shortcode');
add_shortcode('plinks', 'general_link');

//add_shortcode('cat_description2', 'my_cat_description_shortcode2');

add_action('woocommerce_after_single_product','recentview');

add_action('woocommerce_after_add_to_cart_button','cmk_additional_button');
add_action('woocommerce_before_variations_form','show_big_variation');

function recentview(){
    echo  '<h2>Best Selling Products</h2>';
     echo do_shortcode("[best_selling_products per_page='3']");
}
function cmk_additional_button() {
    if ( !WC()->cart->get_cart_contents_count() == 0 ) {
        // Do something fun
        echo '<a href="http://'.$_SERVER["SERVER_NAME"].'/checkout/" class="button" style="float:right;">Check Out</a>';
}
	
}

function show_big_variation() {
  
        // Show image at the top of the variation
        echo '<div class="color_image" style="height:70px;width:100%;"><div class="color_image1" style="width:100%; height:65px;text-align:center;"></div></div>';

	
}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
/***********MODIFICATIONS END***********************************/
add_image_size( 'wordpress-thumbnail', 200, 200, FALSE );


add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    //unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    //unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}

?>